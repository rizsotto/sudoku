package com.gitlab.rizsotto.sudoku

import com.gitlab.rizsotto.sudoku.io.Serializer
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.assertFalse
import kotlin.test.assertNull

class SudokuTest {
    @Test
    fun `index bound check works`() {
        assertFailsWith(IllegalArgumentException::class) {
            Index(row = 0u, col = 1u)
        }
        assertFailsWith(IllegalArgumentException::class) {
            Index(row = 1u, col = 0u)
        }
        assertFailsWith(IllegalArgumentException::class) {
            Index(row = 12u, col = 2u)
        }
        assertFailsWith(IllegalArgumentException::class) {
            Index(row = 2u, col = 23u)
        }
        Index(row = 1u, col = 1u)
        Index(row = 1u, col = 9u)
        Index(row = 9u, col = 1u)
        Index(row = 9u, col = 9u)
        Index(row = 5u, col = 6u)
    }

    @Test
    fun `row group is generated correctly`() {
        val sut = Index(2u, 5u)

        assertEquals(
            expected =
                listOf(
                    Index(row = 2u, col = 1u),
                    Index(row = 2u, col = 2u),
                    Index(row = 2u, col = 3u),
                    Index(row = 2u, col = 4u),
                    Index(row = 2u, col = 6u),
                    Index(row = 2u, col = 7u),
                    Index(row = 2u, col = 8u),
                    Index(row = 2u, col = 9u),
                ),
            actual = sut.row().toList(),
        )
    }

    @Test
    fun `col group is generated correctly`() {
        val sut = Index(2u, 5u)

        assertEquals(
            expected =
                listOf(
                    Index(row = 1u, col = 5u),
                    Index(row = 3u, col = 5u),
                    Index(row = 4u, col = 5u),
                    Index(row = 5u, col = 5u),
                    Index(row = 6u, col = 5u),
                    Index(row = 7u, col = 5u),
                    Index(row = 8u, col = 5u),
                    Index(row = 9u, col = 5u),
                ),
            actual = sut.column().toList(),
        )
    }

    @Test
    fun `sqr group is generated correctly`() {
        val sut = Index(2u, 5u)

        assertEquals(
            expected =
                listOf(
                    Index(row = 1u, col = 4u),
                    Index(row = 1u, col = 5u),
                    Index(row = 1u, col = 6u),
                    Index(row = 2u, col = 4u),
                    Index(row = 2u, col = 6u),
                    Index(row = 3u, col = 4u),
                    Index(row = 3u, col = 5u),
                    Index(row = 3u, col = 6u),
                ),
            actual = sut.square().toList(),
        )
    }

    @Test
    fun `get and set works`() {
        val sut = SudokuImpl()

        assertNull(sut.get(Index(1u, 1u)))
        sut.set(Index(1u, 1u), Value(1))
        assertEquals(Value(1), sut.get(Index(1u, 1u)))

        assertNull(sut.get(Index(3u, 5u)))
        sut.set(Index(3u, 5u), Value(1))
        assertEquals(Value(1), sut.get(Index(3u, 5u)))

        assertEquals(Value(1), sut.get(Index(1u, 1u)))
    }

    @Test
    fun `copy does not change when original mutated`() {
        val original = SudokuImpl()

        original.set(Index(1u, 1u), Value(1))
        original.set(Index(3u, 5u), Value(1))

        val copy = original.copy()

        assertEquals(Value(1), copy.get(Index(1u, 1u)))
        assertEquals(Value(1), copy.get(Index(3u, 5u)))

        original.set(Index(7u, 4u), Value(1))

        assertEquals(Value(1), original.get(Index(7u, 4u)))
        assertNull(copy.get(Index(7u, 4u)))
    }

    @Test
    fun `complete only if it is full`() {
        val sut = SudokuImpl()

        assertFalse(sut.complete())
    }

    @Test
    fun `serializer reads the puzzle`() {
        val sut =
            Serializer.decode(
                """
                [
                    [   5 ,  4 ,null,null,null,null,  1 ,  2 ,null],
                    [ null,null,  7 ,  1 ,null,null,null,null,null],
                    [   8 ,null,null,  5 ,null,  4 ,null,null,  9 ],
                    [ null,  9 ,null,null,null,null,null,  5 ,  4 ],
                    [ null,null,null,  9 ,null,  8 ,null,null,null],
                    [   4 ,  8 ,null,null,null,null,null,  9 ,null],
                    [   6 ,null,null,  8 ,null,  2 ,null,null,  3 ],
                    [ null,null,null,null,null,  3 ,  9 ,null,null],
                    [ null,  3 ,  9 ,null,null,null,null,  8 ,  6 ]
                ]
                """.trimIndent(),
            )

        assertNull(sut.get(Index(2u, 1u)))
        assertEquals(Value(4), sut.get(Index(1u, 2u)))
        assertEquals(Value(7), sut.get(Index(2u, 3u)))
    }
}
