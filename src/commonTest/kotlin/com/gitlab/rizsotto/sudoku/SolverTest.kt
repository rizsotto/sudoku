package com.gitlab.rizsotto.sudoku

import com.gitlab.rizsotto.sudoku.io.Serializer
import kotlin.test.Test
import kotlin.test.assertEquals

class SolverTest {
    @Test
    fun `improve finds new numbers`() {
        val start =
            Serializer.decode(
                """
                [
                    [   5 ,  4 ,null,null,null,null,  1 ,  2 ,null],
                    [ null,null,  7 ,  1 ,null,null,null,null,null],
                    [   8 ,null,null,  5 ,null,  4 ,null,null,  9 ],
                    [ null,  9 ,null,null,null,null,null,  5 ,  4 ],
                    [ null,null,null,  9 ,null,  8 ,null,null,null],
                    [   4 ,  8 ,null,null,null,null,null,  9 ,null],
                    [   6 ,null,null,  8 ,null,  2 ,null,null,  3 ],
                    [ null,null,null,null,null,  3 ,  9 ,null,null],
                    [ null,  3 ,  9 ,null,null,null,null,  8 ,  6 ]
                ]
                """.trimIndent(),
            )
        val expected =
            Serializer.decode(
                """
                [
                    [  5 ,  4 ,  6 ,null,null,  9 ,  1 ,  2 ,  8 ],
                    [  9 ,  2 ,  7 ,  1 ,  8 ,  6 ,null,null,  5 ],
                    [  8 ,  1 ,  3 ,  5 ,  2 ,  4 ,  7 ,  6 ,  9 ],
                    [null,  9 ,null,null,null,null,  8 ,  5 ,  4 ],
                    [null,  6 ,  5 ,  9 ,  4 ,  8 ,null,null,null],
                    [  4 ,  8 ,null,null,null,null,  6 ,  9 ,null],
                    [  6 ,  7 ,  4 ,  8 ,  9 ,  2 ,  5 ,  1 ,  3 ],
                    [null,  5 ,  8 ,null,null,  3 ,  9 ,null,null],
                    [null,  3 ,  9 ,null,null,null,null,  8 ,  6 ]
                ]
                """.trimIndent(),
            )

        Solver.improve(start)
        assertEquals(expected, start)
    }

    @Test
    fun `solve finish the game`() {
        val start =
            Serializer.decode(
                """
                [
                    [   5 ,  4 ,null,null,null,null,  1 ,  2 ,null],
                    [ null,null,  7 ,  1 ,null,null,null,null,null],
                    [   8 ,null,null,  5 ,null,  4 ,null,null,  9 ],
                    [ null,  9 ,null,null,null,null,null,  5 ,  4 ],
                    [ null,null,null,  9 ,null,  8 ,null,null,null],
                    [   4 ,  8 ,null,null,null,null,null,  9 ,null],
                    [   6 ,null,null,  8 ,null,  2 ,null,null,  3 ],
                    [ null,null,null,null,null,  3 ,  9 ,null,null],
                    [ null,  3 ,  9 ,null,null,null,null,  8 ,  6 ]
                ]
                """.trimIndent(),
            )
        val expected =
            Serializer.decode(
                """
                [
                    [ 5, 4, 6, 3, 7, 9, 1, 2, 8],
                    [ 9, 2, 7, 1, 8, 6, 3, 4, 5],
                    [ 8, 1, 3, 5, 2, 4, 7, 6, 9],
                    [ 3, 9, 2, 6, 1, 7, 8, 5, 4],
                    [ 7, 6, 5, 9, 4, 8, 2, 3, 1],
                    [ 4, 8, 1, 2, 3, 5, 6, 9, 7],
                    [ 6, 7, 4, 8, 9, 2, 5, 1, 3],
                    [ 1, 5, 8, 4, 6, 3, 9, 7, 2],
                    [ 2, 3, 9, 7, 5, 1, 4, 8, 6]
                ]
                """.trimIndent(),
            )

        assertEquals(expected, Solver.solve(start))
    }
}
