/*
    sudoku
    Copyright (C) 2021  László Nagy

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import com.gitlab.rizsotto.sudoku.Solver
import com.gitlab.rizsotto.sudoku.io.Serializer
import kotlinx.cinterop.ByteVar
import kotlinx.cinterop.ExperimentalForeignApi
import kotlinx.cinterop.allocArray
import kotlinx.cinterop.memScoped
import kotlinx.cinterop.toKString
import platform.posix.EOF
import platform.posix.EXIT_FAILURE
import platform.posix.EXIT_SUCCESS
import platform.posix.exit
import platform.posix.fclose
import platform.posix.fgets
import platform.posix.fopen
import platform.posix.fputs

fun main(args: Array<String>) {
    if (args.isEmpty()) {
        println("sudoku: missing argument: input file name")
        exit(EXIT_FAILURE)
    }
    try {
        val input = args[0]
        val content = readAllText(input)
        val sudoku = Serializer.decode(content)

        println("input:\n$sudoku")
        val solution = Solver.solve(sudoku)
        println("output:\n$solution")

        if (args.size > 1) {
            val output = args[1]
            val json = Serializer.encode(solution)
            writeAllText(output, json)
        }
    } catch (error: Exception) {
        println("sudoku: error: ${error.message}")
        exit(EXIT_FAILURE)
    }
    exit(EXIT_SUCCESS)
}

fun readAllText(filePath: String): String {
    val returnBuffer = StringBuilder()

    @OptIn(ExperimentalForeignApi::class)
    val file =
        fopen(filePath, READ_MODE)
            ?: throw IllegalArgumentException("Cannot open input file $filePath")

    try {
        @OptIn(ExperimentalForeignApi::class)
        memScoped {
            val readBufferLength = 64 * 1024

            @OptIn(ExperimentalForeignApi::class)
            val buffer = allocArray<ByteVar>(readBufferLength)

            @OptIn(ExperimentalForeignApi::class)
            var line = fgets(buffer, readBufferLength, file)?.toKString()
            while (line != null) {
                returnBuffer.append(line)

                @OptIn(ExperimentalForeignApi::class)
                line = fgets(buffer, readBufferLength, file)?.toKString()
            }
        }
    } finally {
        @OptIn(ExperimentalForeignApi::class)
        fclose(file)
    }

    return returnBuffer.toString()
}

fun writeAllText(
    filePath: String,
    text: String,
) {
    @OptIn(ExperimentalForeignApi::class)
    val file =
        fopen(filePath, WRITE_MODE)
            ?: throw IllegalArgumentException("Cannot open output file $filePath")
    try {
        @OptIn(ExperimentalForeignApi::class)
        memScoped {
            @OptIn(ExperimentalForeignApi::class)
            if (fputs(text, file) == EOF) throw Error("File write error")
        }
    } finally {
        @OptIn(ExperimentalForeignApi::class)
        fclose(file)
    }
}

const val READ_MODE = "r"
const val WRITE_MODE = "w"
