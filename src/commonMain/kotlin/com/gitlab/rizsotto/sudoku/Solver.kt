/*
    sudoku
    Copyright (C) 2021  László Nagy

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.gitlab.rizsotto.sudoku

// Choice represent a pick of a value to a sudoku grid index.
// (Represent your intent to write the number into the cell.)
typealias Choice = Pair<Index, Value>

// A sudoku solver which using a naive algorithm to solve a sudoku game.
object Solver {
    // The method solves the sudoku with a BFS (breadth first search),
    // where the graph nodes are sudoku grids with a choice. The edges
    // are the possible improvements (finding the straight choices) which
    // are coming from the initial choice.
    fun solve(sudoku: Sudoku): Sudoku {
        val nodes: MutableList<Pair<Sudoku, Choice>> =
            improve(sudoku)
                .map { sudoku.copy() to it }
                .toMutableList()
        // early exit if one improvement solved it
        if (sudoku.complete()) {
            return sudoku
        }
        // BFS search consume the not visited nodes.
        while (nodes.isNotEmpty()) {
            // make the choice and improve the game.
            val (current, move) = nodes.removeFirst()
            current.set(move.first, move.second)
            val next = improve(current)
            // exit if the game is solved.
            if (current.complete()) {
                return current
            } else {
                // BFS wants the new choices at the end of the existing list.
                nodes.addAll(next.map { current.copy() to it })
            }
        }
        // could not solve the game.
        throw RuntimeException("Could not solve the game.")
    }

    // The method will improve the sudoku until there are choices,
    // which are single choices. And it returns all choices which
    // are not single choices.
    fun improve(sudoku: Sudoku): List<Choice> {
        val candidates = Candidates(sudoku)
        while (true) {
            val improvement = candidates.findSingleCandidate()
            if (improvement != null) {
                val (index, value) = improvement
                sudoku.set(index, value)
                candidates.resolve(index, value)
            } else {
                break
            }
        }
        return candidates.findMultipleCandidates()
    }
}

class Candidates {
    private val candidatesByIndex: MutableMap<Index, MutableSet<Value>> =
        Index
            .all()
            .associateWith { Value.values().toMutableSet() }
            .toMutableMap()

    private val empties: MutableSet<Index> =
        Index
            .all()
            .toMutableSet()

    constructor(sudoku: Sudoku) {
        Index
            .all()
            .forEach {
                val value = sudoku.get(it)
                if (value != null) {
                    resolve(it, value)
                }
            }
    }

    fun resolve(
        index: Index,
        value: Value,
    ) {
        candidatesByIndex[index] = mutableSetOf(value)
        empties.remove(index)
        // remove the value from candidates in the same groups
        for (group in index.groups()) {
            for (pos in group) {
                candidatesByIndex[pos]?.remove(value)
            }
        }
    }

    fun findSingleCandidate(): Choice? {
        // recalculate the candidates based on if a cell contains a candidate
        // which is not in other cells in the groups, it becomes the only
        // candidate for that cell.
        for (pos in empties) {
            val candidates = candidatesByIndex[pos]!!
            if (candidates.size == 1) {
                return Pair(pos, candidates.first())
            }
            for (group in pos.groups()) {
                val values = group.fold(setOf<Value>()) { acc, current -> acc.union(candidatesByIndex[current]!!) }
                for (candidate in candidates) {
                    if (!values.contains(candidate)) {
                        return Pair(pos, candidate)
                    }
                }
            }
        }
        return null
    }

    fun findMultipleCandidates(): List<Choice> =
        empties.flatMap { idx ->
            val candidates = candidatesByIndex[idx]!!
            candidates.associateBy { idx }.toList()
        }
}

fun Index.groups(): Sequence<Group> = sequenceOf(this.row(), this.column(), this.square())
