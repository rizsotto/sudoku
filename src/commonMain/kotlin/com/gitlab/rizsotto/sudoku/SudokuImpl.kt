/*
    sudoku
    Copyright (C) 2021  László Nagy

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.gitlab.rizsotto.sudoku

// Implementation based on a single array of values.
class SudokuImpl : Sudoku {
    private val values: Array<Value?>

    constructor() {
        values = arrayOfNulls(9 * 9)
    }

    constructor(init: Array<Value?>) {
        values = init.copyOf()

        if (values.size != 81) {
            throw IllegalArgumentException("sudoku grid size must be 9x9")
        }
    }

    constructor(init: List<List<Int?>>) {
        values =
            init
                .flatten()
                .map { number -> number?.let { Value(it) } }
                .toTypedArray()

        if (values.size != 81) {
            throw IllegalArgumentException("sudoku grid size must be 9x9")
        }
    }

    override fun get(index: Index): Value? {
        val idx = ((index.row - 1u) * 9u) + (index.col - 1u)
        return values[idx.toInt()]
    }

    override fun set(
        index: Index,
        value: Value,
    ) {
        val idx = ((index.row - 1u) * 9u) + (index.col - 1u)
        values[idx.toInt()] = value
    }

    override fun complete(): Boolean = Index.all().filter { this.get(it) == null }.none()

    override fun copy(): Sudoku = SudokuImpl(this.values)

    override fun toString(): String {
        val builder = StringBuilder()
        for (row in 1u..9u) {
            for (col in 1u..9u) {
                val index = Index(row, col)
                val value = this.get(index)
                builder.append(" ${value?.v ?: " "} ")
                builder.append(",")
            }
            builder.append('\n')
        }
        return builder.toString()
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        return other is SudokuImpl && this.values.contentEquals(other.values)
    }

    override fun hashCode(): Int = values.contentHashCode()
}

fun Sudoku.toListOfInts(): List<List<Int?>> =
    Index(1u, 1u)
        .column(include = true)
        .map { row ->
            row
                .row(include = true)
                .map { idx ->
                    this.get(idx)?.v
                }.toList()
        }.toList()
