/*
    sudoku
    Copyright (C) 2021  László Nagy

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.gitlab.rizsotto.sudoku

// This is the abstraction of a 9x9 grid which represents the game.
//   https://en.wikipedia.org/wiki/Sudoku
interface Sudoku {
    fun get(index: Index): Value?

    fun set(
        index: Index,
        value: Value,
    )

    fun complete(): Boolean

    fun copy(): Sudoku
}

// This represents the digits which is used by the game.
value class Value(
    val v: Int,
) {
    init {
        require(v in 1..9) { "value should be a number from 1 to 9" }
    }

    companion object {
        fun values(): Sequence<Value> =
            sequence { yieldAll(1..9) }
                .map { Value(it) }
    }
}

// Represents a position in the grid.
data class Index(
    val row: UInt,
    val col: UInt,
) {
    init {
        require(row in 1u..9u) { "row should be a number from 1 to 9" }
        require(col in 1u..9u) { "col should be a number from 1 to 9" }
    }

    companion object {
        fun all(): Group =
            sequence {
                for (row in 1u..9u) {
                    for (col in 1u..9u) {
                        yield(Index(row, col))
                    }
                }
            }
    }
}

// Group represents the cells which are belongs together and have a constraint
// to have all values in it.
typealias Group = Sequence<Index>

fun Index.row(include: Boolean = false): Group =
    sequence { yieldAll(1u..9u) }
        .map { Index(row = this.row, col = it) }
        .filter { include || it != this }

fun Index.column(include: Boolean = false): Group =
    sequence { yieldAll(1u..9u) }
        .map { Index(row = it, col = this.col) }
        .filter { include || it != this }

fun Index.square(include: Boolean = false): Group {
    val rowOffset: UInt = ((this.row - 1u) / 3u) * 3u
    val colOffset: UInt = ((this.col - 1u) / 3u) * 3u
    return sequenceOf(
        Index(1u, 1u), Index(1u, 2u), Index(1u, 3u),
        Index(2u, 1u), Index(2u, 2u), Index(2u, 3u),
        Index(3u, 1u), Index(3u, 2u), Index(3u, 3u),
    ).map {
        Index(row = (it.row + rowOffset), col = (it.col + colOffset))
    }.filter { include || it != this }
}
