/*
    sudoku
    Copyright (C) 2021  László Nagy

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.gitlab.rizsotto.sudoku.io

import com.gitlab.rizsotto.sudoku.Sudoku
import com.gitlab.rizsotto.sudoku.SudokuImpl
import com.gitlab.rizsotto.sudoku.toListOfInts
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json

typealias Puzzle = List<List<Int?>>

object Serializer {
    fun decode(input: String): Sudoku =
        Json
            .decodeFromString<Puzzle>(input)
            .let { SudokuImpl(it) }

    fun encode(input: Sudoku): String =
        input
            .toListOfInts()
            .let { Json.encodeToString(it) }
}
