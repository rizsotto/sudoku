# Sudoku solver

This is a [sudoku](https://en.wikipedia.org/wiki/Sudoku) solver, which is using a
naive algorithm to find a solution to the puzzle. It reads the puzzle from a JSON
file and outputs a JSON file with the solution (if found).

Wrote as a toy project to get familiar with Kotlin native development environment.

## how to build

The following command will build the project executables.

    ./gradlew build --info

The following command will run the project test.

    ./gradlew check --info

## how to use

Find the built executable and run as...

    sudoku <input file name> <output file name>
