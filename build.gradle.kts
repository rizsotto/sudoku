plugins {
    kotlin("multiplatform") version "1.9.20"
    kotlin("plugin.serialization") version "1.9.20"
    id("com.diffplug.spotless") version "6.22.0"
}

group = "com.gitlab.rizsotto"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

kotlin {
    linuxX64 {
        binaries {
            executable()
        }
    }

    sourceSets {
        val commonMain by getting {
            dependencies {
                implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:1.6.0")
            }
        }
    }
}

spotless {
    kotlin {
        target("**/*.kt")

        ktlint()
    }
    kotlinGradle {
        target("**/build.gradle.kts")

        ktlint()
    }
    format("misc") {
        target("*.md", ".gitignore")

        trimTrailingWhitespace()
        endWithNewline()
    }
}
